# Machine learning codes 

Tatiane Franco Machado (tatiane.machado@wikki.com.br), Software Engineer at Wikki Brasil.

## Sobre

Repositório para armazenar e melhorar códigos de inteligência artifical ao longo do curso. 
A cada tópico já estudado e com mapa mental construído, os quadrados abaixo serão marcados, portanto, todos os tópicos marcados, terão seus códigos disponíveis para download.

- Tópicos do curso:

1) Módulo 1

- [x] Regressão Linear [Mapa mental][(https://mm.tt/2244109959?t=HVjcPlTW0U)]
- [ ] Pré-processamento
- [ ] Outros modelos de regressão linear
- [ ] Validação cruzada e ajuste fino dos parâmetros
- [ ] Regressão Logística
- [ ] Confusion matrix e normalização
- [ ] KNN
- [ ] Naive Bayes
- [ ] Decision Trees
- [ ] Feature Selection
- [ ] Gradient Boosting


## Requisitos

- Python 3.8 ou Jupyter-notebook 6.0

## Uso

### Opção 1: 

- Para utilizar os códigos basta fazer o download e usar qualquer IDE que compile códigos em python 

### Opção 2: 

- Rodar o arquivo no terminal:

```
$ python3 nome_do_codigo.py

```


