#!/usr/bin/env python
# coding: utf-8

# In[1]:


## Libraries 

from sklearn.datasets import make_regression
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import numpy as np
from sklearn.model_selection import train_test_split

## Creating data form regression
## n_samples = number of points
## n_features = dimensions (1=linear)
## noise = how much it differs from the regression

x, y = make_regression(n_samples=1000, n_features =1, noise =20)

plt.scatter(x,y)
plt.show()


# In[2]:


## Calling the object from model
model = LinearRegression()

## Create a model to represent the data 
model.fit(x,y)

## Get the parameters
## y = ax + b

a = model.coef_
b = model.intercept_


print('The linear coefficient of the model is: ', b)
print(f'The angular coefficient of the model is: {a}')


# In[3]:


## Plotting the results
def plotScatter(x,y,a,b):
    plt.scatter(x,y)
    xreg = np.arange(-4,4,1) #or xreg = x
    yreg = a*xreg + b
    plt.plot(xreg,yreg,color='red')
    plt.show()

plotScatter(x,y,a,b)


# In[9]:


## Splitting data into train and test
## x = complete data from x
## y = complete data from y
## test_size = percentual of data that is used to test the model

x_train, x_test, y_train, y_test = train_test_split(x, y,test_size=0.33)

print('Length of data for training: ', len(x_train))
print('Length of data for testing: ', len(x_test))


# In[10]:


## Creating the model to fit train data
model.fit(x_train,y_train)

a1 = model.coef_
b1 = model.intercept_

print('The linear coefficient of the model is: ', b1)
print(f'The angular coefficient of the model is: {a1}')



# In[11]:


## Basic mesure of model quality for test data
R2_test = model.score(x_test,y_test)
R2_train = model.score(x_train,y_train)
print('The R² of the model for test data is: ', R2_test)
print('The R² of the model for train data is: ', R2_train)


# In[12]:


## Plotting test data

plotScatter(x_test,y_test,a1,b1)


# In[8]:


## Plotting train data

plotScatter(x_train,y_train,a1,b1)

